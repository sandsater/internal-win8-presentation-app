﻿using System;

public class FakeCMS : ICMS
{
    public FakeCMS()
    {

    }


    public string getLatestVersionLink()
    {
        //return @"https://drive.google.com/file/d/0B9qzsVzrUUI5WDlZemx5SE80OE0/view?usp=sharing";
        return @"c:\v3.20.zip";
    }

    public int getLatestVersion()
    {
       return FakeServer.Properties.Settings.Default.version;
    }

}

