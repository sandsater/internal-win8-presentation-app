﻿public interface ICMS
{
    string getLatestVersionLink();
    int getLatestVersion();
}