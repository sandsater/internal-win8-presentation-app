﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Web.Script.Serialization;

namespace m2_retail_lab1
{

	public class Manage
	{
		private const string CONTENT_PATH = @"C:\M2-retail\Content\";
		private const string ZIP_NAME = "latest.zip";
		private const string TEMP_EXTRACT_FOLDER = "latest";
		private const string JSON_INFO_FILE_NAME = "info.json";

		static ICMS cms = new FakeCMS();

		public static void setClientVersionToRun(int version)
		{
			Properties.Settings.Default.currentRunVersion = version;
			Properties.Settings.Default.Save();
		}

		public static int getClientRunVersion()
		{
			return Properties.Settings.Default.currentRunVersion;
		}

		public static bool updateExists()
		{
			int serverVersion = getLatestServerVersion();
			if (clientVersionExists(serverVersion))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		private static bool clientVersionExists(int version)
		{
			IList<String> availableVersions = listVersions();
			return availableVersions.Contains(version.ToString());
		}

		private static int getLatestServerVersion()
		{
			return cms.getLatestVersion();
		}

		public static bool getUpdate()
		{
			try
			{
				string downloadLink = cms.getLatestVersionLink();

				downloadZipfile(downloadLink);
				extractZip();
				int newVersion = extractLatestVersionFromJson();
				setClientVersionToRun(newVersion);
				cleanupDownloads(newVersion);

				return true;
			}
			catch
			{
				return false;
			}
		}

		private static void downloadZipfile(string url)
		{
			using (WebClient myWebClient = new WebClient())
			{
				myWebClient.DownloadFile(url, buildZipPath());
			}

		}

		private static void extractZip()
		{
			using (FileStream zipToExtract = new FileStream(buildZipPath(), FileMode.Open))
			{
				using (ZipArchive archive = new ZipArchive(zipToExtract, ZipArchiveMode.Read))
				{
					archive.ExtractToDirectory(buildExtractPath());
				}
			}
		}

		private static int extractLatestVersionFromJson()
		{
			JsonInfo info = new JsonInfo();
			using(StreamReader sr = new StreamReader(buildJsonPath()))
			{
				string jsonString = sr.ReadToEnd();
				JavaScriptSerializer ser = new JavaScriptSerializer();
				info = ser.Deserialize<JsonInfo>(jsonString);
			}
			return info.version;
		}

		private static void cleanupDownloads(int version)
		{
			File.Delete(buildZipPath());
			Directory.Move(buildExtractPath(), buildDirPath(version));
		}


		public static IList<String> listVersions()
		{
			IList<String> versionList = new List<String>();

			DirectoryInfo contentFolder = new DirectoryInfo(CONTENT_PATH);
			foreach (DirectoryInfo dir in contentFolder.GetDirectories())
			{
				versionList.Add(dir.Name);
			}

			return versionList;
		}

		public static void deleteVersion(int version)
		{
			if (getClientRunVersion() != version)
			{
				Directory.Delete(buildDirPath(version), true);
			}
		}

		private static string buildDirPath(int version)
		{
			return Path.Combine(CONTENT_PATH, version.ToString());
		}

		private static string buildZipPath()
		{
			return Path.Combine(CONTENT_PATH, ZIP_NAME);
		}

		private static string buildExtractPath()
		{
			return Path.Combine(CONTENT_PATH, TEMP_EXTRACT_FOLDER);
		}

		private static string buildJsonPath()
		{
			return Path.Combine(CONTENT_PATH, TEMP_EXTRACT_FOLDER, JSON_INFO_FILE_NAME);
		}

	}
}