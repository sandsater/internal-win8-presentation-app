﻿using System.Windows;



namespace m2_retail_lab1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            tbClientVersion.Text = Manage.getClientRunVersion().ToString();

            if (Manage.updateExists())
            {
                btnUpdate.IsEnabled = true;
            }
            else
            {
                btnUpdate.IsEnabled = false;
            }

            LoadVersionList();
        }


        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (Manage.getUpdate())
            {
                tbClientVersion.Text = Manage.getClientRunVersion().ToString();
                SetUpdateButton();
				LoadVersionList();
            }
            else
            {
                MessageBox.Show("Update failed, please try again later");
            }


        }

        private void SetUpdateButton(){
            if (Manage.updateExists())
            {
                btnUpdate.IsEnabled = true;
            }
            else
            {
                btnUpdate.IsEnabled = false;
            }
        }

        private void LoadVersionList()
        {
            lbVersionList.ItemsSource = Manage.listVersions();
        }

        private void btnDeleteVersion_Click(object sender, RoutedEventArgs e)
        {
            object selectedItem = lbVersionList.SelectedItem;
            
            if (selectedItem != null) 
            {
                if(MessageBox.Show("Do you want to delete the selected version?", "Confirm", MessageBoxButton.OKCancel) == MessageBoxResult.OK){
                    Manage.deleteVersion(int.Parse(selectedItem.ToString()));
                    LoadVersionList();
                }
            }
        }

		private void btnSetVersion_Click(object sender, RoutedEventArgs e)
		{
			object selectedItem = lbVersionList.SelectedItem;

			if (selectedItem != null)
			{
				if (MessageBox.Show("Do you want to set the selected version to your current?", "Confirm", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
				{
					Manage.setClientVersionToRun(int.Parse(selectedItem.ToString()));
					tbClientVersion.Text = Manage.getClientRunVersion().ToString();
				}
			}
		}

        
       

    }
}
